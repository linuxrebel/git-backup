# Git backup and Sync scripts

## gitSync-All

* This goes into your $HOME/bin folder, or anywhere in your path. It will look in $HOME/git for folders

## gitSync

*  This goes into any subfolder under your $HOME/git (Not actual git repos)
*  So that if you have a directory structure like this 
  * $HOME/git/3rdparty
  * $HOME/git/my_git_repos
  * $HOME/git/work_git_repos 
*  You would put gitSync in each of the 3 directories above.  If you have a 4th dir liek $HOME/git/stuff_I_keep_local. and you don't want it synced.  Don't put a copy of gitSync in that dir

## bkpGit

  This is a script to backup your git repos to a simple txt file that is a list of all of the URLS you need to restore your git repos by cloneing them again from your online repositories. Especially usefull in keeping backups of your $HOME in case of rebuild/re-install, or if you have multiple systems (laptop and desktop for example) you want to keep in sync.

### This is the meat of the backup and restore

  1. Edit the script to change the variable $ngdir, to the path for the folder you will keep the backup list in
  2. Put this script in your $HOME/bin dir or somewhere in your path.
  3. It assumes your repos are in $HOME/git
  4. Run the scrip, it will ask if you are doing a backup or a restore
  5. Enter a 1 for backup or 2 for restore (anything else it yells at you)
  6. If you choose backup it will write a file into your backup dir called git-urls.list
  7. If it finds a previous copy of that file it will put it into git-urls.list.bkp and then create a new clean copy
  8. If you choose restore.  It will parse the git-urls.list file and recreate and clone all of the listed git repos 

*NOTE: Not only does it recreate the repos, but it also re-creates the dir stucture.  Be careful at this time I've not tested it yet (I will) on situations where there are not sub-dirs under $HOME/git*
